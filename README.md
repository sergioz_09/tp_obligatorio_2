# README #

Codigo fuente del trabajo practico obligatorio N°2
 

### Instalacion ###
Este proyecto usa una libreria externa de Apache 'commons-Math' 
http://commons.apache.org/proper/commons-math/userguide/index.html
Integrada en el proyecto.



### Como usarlo? ###

la clase MinimosCuadrados.java tiene un metodo estatico publico llamado "solve". 

Donde se le pasa el grado del polinomio, el valor de x (punto a estimar), un arreglo de puntos.

Retorna un Double que es la solucion.

### Resolucion de ejercicios###
En /src/tp/obligatorio/dos/MinimosCuadradosTest.java se resuelven los ejercicios 6 y 7.

Se imprimen en pantalla respectivamente.



### Notas ###
 Para la resolucion de los ejercicios 6 y 7, el grado del polinomio fue elegido según la grafica de los puntos.

El paquete "tp.obligatorio.dos" contiene el codigo fuente hecho por nosotros.



### Integrantes ###
- Juan Lucero
- Nacho Cayuqueo
- Ricardo Sergio