package tp.obligatorio.dos;

import org.apache.commons.math3.linear.*;

/**
 * Mínimos cuadrados es una técnica de análisis numérico enmarcada dentro de la optimización matemática,
 * en la que, dados un conjunto de pares ordenados y una familia de funciones, se intenta encontrar la función continua,
 * dentro de dicha familia, que mejor se aproxime a los datos.
 */
public class MinimosCuadrados {
    /**
     * Resulve por minimos cuadrados
     * @param grado El grado del polinomio
     * @param valor El valor de x.
     * @return El valor obtenido, producto de substituir x en un polinomio de grado 'n'
     */
    public static double solve(int grado, double x, Punto[] puntos) {
        double[][] realMatrix =  new double[grado + 1][grado + 1]; // Matriz de coeficientes
        double[] arrayRealVector = new double[grado + 1]; // Matriz de incognitas
        for(int j = 0; j <= grado; j++) {
            realMatrix[j] = getLinearEcuation(grado, j, puntos); // Arma la ecuacion j.
            arrayRealVector[j] = getConstant(j, puntos); // Obtiene el valor numerico de la constante.
        }
        // Utilizando apache commons [Math] resolvemos el sitema de ecuaciones.
        DecompositionSolver solver = new LUDecomposition(new Array2DRowRealMatrix(realMatrix, false)).getSolver();
        RealVector constants = new ArrayRealVector(arrayRealVector, false);
        // Resuelve el sistema de ecuaciones y con la solucion obtenida remplaza el valor de x en el polinomio de grado 'n'
        return getResult(solver.solve(constants), grado, x);
    }

    /**
     * Obtiene el termino independiente para la ecuacion 'n'
     * @param n
     * @return
     */
    private static double getConstant(int n, Punto[] puntos) {
        double resultado = 0.0;
        for (int i = 0; i < puntos.length; i++) {
            resultado = resultado + ( Math.pow(puntos[i].x, n) * puntos[i].fx);
        }
        return resultado;
    }


    /**
     * Arma la ecuacion linear segun el algoritmo de minimos cuadrados.
     * @param grado
     * @param i
     * @return
     */
    private static double[] getLinearEcuation(int grado, int i, Punto[] puntos) {
        double[] ecuacion = new double[grado + 1];
        for (int k = 0; k <= grado; k++) {
            ecuacion[k] = (i == 0 && k == 0)? puntos.length : sumatoriaX(k + i, puntos);
        }
        return ecuacion;
    }

    /**
     * Formula para obtener el valor del coeficiente.
     * @param exponente
     * @param grado
     * @return
     */
    private static double sumatoriaX(int exponente, Punto[] puntos) {
        double result = 0;
        for (int i = 0; i < puntos.length; i++) {
            result = result + Math.pow(puntos[i].x, exponente);
        }
        return result;
    }

    /**
     * Arma un polinomio de grado 'n' cuyos coeficientes son las soluciones(respectivamente) . Luego reemplaza el valor de x en el polinomio obtenido.
     * @param soluciones Los coeficientes
     * @param grado El grado del polinomio
     * @param x El Valor de la variable x
     * @return
     */
    private static double getResult(RealVector soluciones, int grado, double x) {
        double result = 0;
        for (int i = 0; i <= grado; i++) {
            result = result + ( soluciones.getEntry(i) * Math.pow(x, i));
        }
        return result;
    }
}