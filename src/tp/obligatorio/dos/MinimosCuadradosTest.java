package tp.obligatorio.dos;

public class MinimosCuadradosTest {
    /**
     * Metodo principal que ejecuta los ejercicios.
     * @param args
     */
    public static void main(String[] args) {
    	cargarDatos();
    }

    /**
     * Resolucion del ejercicio n� 6
     */
    public static void ej6() {
        Punto[] puntos = {
                new Punto(55, 14.08),
                new Punto(70, 13.56),
                new Punto(85, 13.28),
                new Punto(100, 12.27),
                new Punto(120, 11.3),
                new Punto(140, 10.4)
        };
        int grado = 1;
        double valor = 80;
        System.out.println(MinimosCuadrados.solve(grado, valor, puntos));
    }

    /**
     * Resolucion del ejercicio n� 7
     */
    public static void ej7() {
        Punto[] puntos = {
                new Punto(-2, -8),
                new Punto(0, 0),
                new Punto(1, 1),
                new Punto(2, 8),
        };
        int grado = 3;
        double valor = -1;
        System.out.println(MinimosCuadrados.solve(grado, valor, puntos));
    }
    
    public static void cargarDatos() {
        System.out.println("Ingrese el grado:");
        int grado = TecladoIn.readInt();
        System.out.println("Ingrese el valor:");
        double valor = TecladoIn.readDouble();
        System.out.println("Ingrese cantidad de puntos:");
        int cantidad = TecladoIn.readInt();
        Punto[] puntos = new Punto[cantidad];  
        for(int i = 0; i < cantidad; i++) {
        	System.out.print("x:");
        	double x = TecladoIn.readDouble();
        	System.out.print("y:");
        	double y = TecladoIn.readDouble();
        	puntos[i] = new Punto(x, y);
        }
        System.out.println("El resultado es: ");
        System.out.println(MinimosCuadrados.solve(grado, valor, puntos));
    }
}